// Wrap in anonymous function to avoid global scope
(function() {

	//
	// Function scope declarations
	//

	var userId    = Math.random();
	var gameId    = "";
	var elContent = document.getElementById("content");
	var userName  = "User";
	var isCreator = false;
	var written   = [];
	var voted     = [];

	//
	// Cache dom
	//

	// Global dom
	var elPages   = document.getElementsByClassName("page");
	var elGameIds = document.getElementsByClassName("game-identifier");

	// Start dom
	var elStartPage           = document.getElementById("start");
	var elGameIdentifierInput = document.getElementById("game-identifier-input");
	var elUserNameInput       = document.getElementById("user-name-input");
	var elCreateGame          = document.getElementById("create-game");
	var elJoinGame            = document.getElementById("join-game");
	var elErrorMessage        = document.getElementById("error-message");

	// Lobby dom
	var elLobbyPage   = document.getElementById("lobby");
	var elUserAction  = document.getElementById("user-action");
	var elStartButton = document.getElementById("start-button");

	//
	// Start page
	//

	elCreateGame.onclick = function(event) {
		gameId = elGameIdentifierInput.value;
		if (gameId.length < 3) {
			elErrorMessage.innerHTML = 'Game identifier must be at least 3 characters long';
			return;
		}
		httpUtils.post("/game/create", { gameId, userId })
			.then(function(result) {
				console.log("Result:", result);
				var success = parseInt(result);
				if (success) {
					isCreator = true;
					elStartPage.style.display = "none";
					elLobbyPage.style.display = "block";
					elUserAction.innerText = "created";
					for (var element of elGameIds) {
						element.innerText = gameId;
					}
				} else {
					elErrorMessage.innerText = `Game identified with ${gameId} already exists`;
				}
			}, errorHandler);
	}

	elJoinGame.onclick = function(event) {
		gameId = elGameIdentifierInput.value;
		userName = elUserNameInput.value;
		if (gameId.length < 3) {
			elErrorMessage.innerHTML = 'Game identifier must be at least 3 characters long';
			return;
		}
		httpUtils.post("/game/join", { gameId, userId, userName })
			.then(function(result) {
				console.log("Result:", result);
				var success = parseInt(result);
				if (success) {
					isCreator = false;
					elStartPage.style.display = "none";
					elLobbyPage.style.display = "block";
					elUserAction.innerText = "joined";
					for (var element of elGameIds) {
						element.innerText = gameId + ' as ' + userName;
					}
				} else {
					elErrorMessage.innerText = `Game identified with ${gameId} does not exist`;
				}
			}, errorHandler);
	}

	//
	// Lobby
	//

	elStartButton.onClick = function(event) {
		httpUtils.post("/game/start", { gameId, userId })
			.then(function(result) {

			}, errorHandler);
	}

	function update() {
		httpUtils.get("/game/state")
			.then(function(result) {
				console.log("Result:", result);
				let currentState = JSON.parse(result);
				switch (currentState.page) {
					case "lobby":
						hideAllPages();
						// Show the lobby
						elLobbyPage.style.display = "block";
						break;
					case "write":
						hideAllPages();
						// Show either the write page or the write wait page depending on if they've written
						if (written.includes(currentState.round)) {
							elWritePage.style.display = "block";
						} else {
							elWriteWaitPage.style.display = "block";
						}
						break;
					case "vote":
						hideAllPages();
						// Show either the vote page or the vote wait page depending on if they've voted
						if (voted.includes(currentState.round)) {
							elVotePage.style.display = "block";
						} else {
							elVoteWaitPage.style.display = "block";
						}
						break;
					case "score":

						break;

				}
			}, errorHandler);
	}

	function hideAllPages() {
		for (var element of elPages) {
			element.style.display = "none";
		}
	}

	function errorHandler(err) {
		console.log(err);
	}
})()
