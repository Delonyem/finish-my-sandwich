var httpUtils = (function() {
	function get(url) {
		return new Promise(function(resolve, reject) {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if (this.status == 200) {
						resolve(this.responseText);
					} else {
						reject(this.status.toString());
					}
				}
			};
			xhttp.open("GET", url, true);
			xhttp.send();
		});
	}

	function post(url, body) {
		return new Promise(function(resolve, reject) {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if (this.status == 200) {
						resolve(this.responseText);
					} else {
						reject(this.status.toString());
					}
				}
			};
			xhttp.open("POST", url, true);
			xhttp.setRequestHeader("Content-type", "application/json")
			xhttp.send(JSON.stringify(body));
		});
	}

	return { get, post };
})();
