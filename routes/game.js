var express = require('express');
var router = express.Router();

var games = {}

/* POST game create listing. */
router.post('/create', function(req, res, next) {
	var data = req.body;

	// Make sure there's no game with this id, and that the id is of sufficient length
	if (!games[data.gameId] && data.gameId.length >= 3) {
		games[data.gameId] = { creator: data.userId, users: [] };
		res.send('1');
	} else {
		res.send('0');
	}
});

/* POST game join listing. */
router.post('/join', function(req, res, next) {
	var data = req.body;

	if (games[data.gameId] && data.gameId.length >= 3) {
		games[data.gameId].users.push({ userId: data.userId, userName: data.userName });
		res.send('1');
	} else {
		res.send('0');
	}
});

module.exports = router;
